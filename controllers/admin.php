<?php  
  class admin extends Controller
  {
      
      function __construct()
      {
           parent::__construct();
      }            
      
      function index()
      {
          if(session::getValue('perfil') == 'administrador')
          {
            $this->view->render('add/head_');
            $this->view->render('add/header_');
            $this->view->render('perfil/admin/index');
            $this->view->render('add/js_');
            $this->view->render('add/footer_');
          }else{
              $this->login();
          }
      }
      function login()
      {    
           session::destroy();                
          $this->view->render('login');
      }  
      
  }
?>