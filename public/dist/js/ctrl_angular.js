var myApp = angular.module('validaciones', [])
myApp.controller('validacion',['$scope','$http', function($scope,$http) {   
  $scope.master = {}; 
  $scope.estilo = '';
  $scope.tipo = '';
  $scope.mess = '';
  // alert-success 
  // $scope.message = '';
  $scope.mensajes = function(dato)
  {
    if(dato == 3)
    {   
      $scope.estilo = '';
      $scope.tipo = '';
      $scope.mess = '';       
      $scope.estilo = 'callout-danger zoomIn animated ';
      $scope.tipo = 'Error';
      $scope.mess = 'Usuario Invalido.';
    }
    if(dato == 1)
    { 
      $scope.estilo = '';
      $scope.tipo = '';
      $scope.mess = '';     
      $scope.estilo = 'callout-success zoomIn animated ';
      $scope.tipo = 'Correcto';
      $scope.mess = 'Bienvenido Usuario.';
    }
  }
  $scope.sinmensaje = function()
  {
    $scope.estilo = '';
    $scope.tipo = '';
    $scope.mess = '';
  }
  $scope.update = function(user) {
    $scope.master = angular.copy(user);

    // codigo url absoluta
      var loc = window.location;
      var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
      var dir = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
     
    $http({
                method: 'POST',             
                url: dir+'user/entrar',              
                data: { nom: $scope.user.nom, pass: $scope.user.pass }
              }).then(function successCallback(response) {                
                if(response.data.tipo == 1){
                    // window.location = 'home.php';
                    // console.log(response.data.tipo);
                    $scope.mensajes(response.data.tipo);
                    setTimeout(function(){ window.location = dir+'admin/index'; }, 1000);
                }else if(response.data.tipo == 3){
                     $scope.mensajes(response.data.tipo);
                }
              }, function errorCallback(response) {
                alert('Error: ' + response);                
              });
  }

}]);
myApp.controller('envio',['$scope','$rootScope','$http', function($scope,$http,$rootScope){ 
        $rootScope.prueba='';  
    //   var loc = window.location;
    //   var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    //   var dir = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
     
    // $http({
    //             method: 'POST',             
    //             url: dir+'user/entrar',              
    //             data: { nom: $scope.user.nom, pass: $scope.user.pass }
    //           }).then(function successCallback(response) {                
    //             if(response.data.tipo == 1){
    //                 // window.location = 'home.php';
    //                 // console.log(response.data.tipo);
    //                 $scope.mensajes(response.data.tipo);
    //                 setTimeout(function(){ window.location = dir+'admin/index'; }, 1000);
    //             }else if(response.data.tipo == 3){
    //                  $scope.mensajes(response.data.tipo);
    //             }
    //           }, function errorCallback(response) {
    //             alert('Error: ' + response);                
    //           });
}]);
myApp.controller('envio1',['$scope','$rootScope','$http', function($scope,$http,$rootScope){ 
        $rootScope.prueba='';  
 
}]);
myApp.controller('envio2',['$scope','$rootScope','$http', function($scope,$http,$rootScope){ 
        $rootScope.prueba='';     
}]);