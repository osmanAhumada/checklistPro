 <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>Pro</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Checklist</b>Pro</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo URL;?>public/dist/img/user.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo session::getValue('nombre');?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo URL;?>public/dist/img/user.png" class="img-circle" alt="User Image">

                <p>
                  <?php echo session::getValue('nombre');?> - Desarrollador Web
                  <small>Desde el. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Seguidores</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Ventas</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Amigos</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo URL?>user/salir" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        <!--  <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
      </div>
    </nav>
  </header>
  
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo URL;?>public/dist/img/user.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo session::getValue('nombre');?></p>
          <a href="#"><?php echo session::getValue('perfil');?></a>
        </div>
      </div>    
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Navegacion Principal</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Escritorio</span>          
          </a>         
        </li>  
         <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Usuarios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Registrar Usuario</a></li>
            <li><a href="<?php echo URL?>modulo/userLista/lista"><i class="fa fa-circle-o"></i>Listar Usuario</a></li>
          </ul>
        </li>   
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>ART</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo URL?>modulo/art/paso1"><i class="fa fa-circle-o"></i>Paso 1</a></li>
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Paso 2</a></li>
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Paso 3</a></li>
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Paso 4</a></li>
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Paso 5</a></li>
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Paso 6</a></li>
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Paso 7</a></li>
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Paso 8</a></li>
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Información Complemetaria</a></li>
          </ul>
        </li> 
         <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>check list</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Registrar Usuario</a></li>
            <li><a href="<?php echo URL?>modulo/userLista/lista"><i class="fa fa-circle-o"></i>Listar Usuario</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Observador de Conducta</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Registrar Usuario</a></li>
            <li><a href="<?php echo URL?>modulo/userLista/lista"><i class="fa fa-circle-o"></i>Listar Usuario</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Protocolo de Fatalidad</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo URL?>modulo/userRegistra"><i class="fa fa-circle-o"></i>Registrar Usuario</a></li>
            <li><a href="<?php echo URL?>modulo/userLista/lista"><i class="fa fa-circle-o"></i>Listar Usuario</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>