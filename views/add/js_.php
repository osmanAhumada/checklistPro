<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <!--<b>Version</b> 2.3.7-->
    </div>
    <strong>&copy; 2016 <a href="#">Checklist<b>Pro</b></a>.</strong>
</footer>
      <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- jQuery 2.2.3 -->
<script src="<?php echo URL?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?php echo URL?>public/dist/js/angular.min.js"></script>
<script src="<?php echo URL?>public/dist/js/ctrl_angular.js"></script>
<script src="<?php echo URL?>public/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo URL?>public/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo URL?>public/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo URL?>public/dist/js/app.js"></script>
<script src="<?php echo URL?>public/dist/js/pages/dashboard.js"></script>
<script>
	   $('input[type="checkbox"].flat-green').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>
