<script>
 $("#guardar_user").click(function(){
      var id = $("#id").val();
      var nombre = $("#nombre").val();
      var usuario = $("#usuario").val();
      var pass = $("#pass").val();
      var perfil = $("#perfil").val();      
     if(nombre != '' && usuario != '' && pass != '')
         {
              var valores = {
                  'id'     : id,
                  'nombre' : nombre,
                  'usuario': usuario,
                  'pass'   : pass,
                  'perfil' : perfil
              };
              $.ajax({
                  type : 'POST',
                  url  :  '<?php echo URL?>user/insert',
                  data : valores,
                  dataType: "json",
                  success : function(data)
                  {              
                      if(data.tipo == 'user_guardado')
                        {
                            swal("Registrado!", "Usuario registrado con exito","success");  
                             $("#nombre").val('');
                             $("#usuario").val('');
                             $("#pass").val('');
                        }              
                      if(data.tipo == 'sin_datos')
                        { 
                            swal("Error!", "Faltan datos que ingresar","error"); 
                        } 
                  },
                  error : function( jqXHR, textStatus, errorThrown ) {

                      if (jqXHR.status === 0) {

                        alert('Not connect: Verify Network.');

                      } else if (jqXHR.status == 404) {

                        alert('Requested page not found [404]');

                      } else if (jqXHR.status == 500) {

                        alert('Internal Server Error [500].');

                      } else if (textStatus === 'parsererror') {

                        alert('Requested JSON parse failed.');

                      } else if (textStatus === 'timeout') {

                        alert('Time out error.');

                      } else if (textStatus === 'abort') {

                        alert('Ajax request aborted.');

                      } else {

                        alert('Uncaught Error: ' + jqXHR.responseText);

                      }
                  }
              }); 
         }else{
            swal("Formulario Incompleto!", "Ingrese todo los datos porfavor","error");  
         }    
    return false;
  }); /* Registrar usuario */
 $(".accion a").click(function(){
     var clave = $( this ).attr('id');     
     var consulta = clave.substring(0,1);
     var id = clave.substring(1);
     var dat = {
         'id' : id
     };   
     if(consulta == 'D')
      {     
         var url = '<?php echo URL?>user/delete';
          $.ajax({
             type : 'POST',
             url  : url,
             data : dat,
             dataType : 'json',
             success : function(datos)
               {
                   if(datos.tipo == 'user_eliminado')
                       {
                            swal({
                              title:"Usuario Eliminado!",
                              text: "Usuario removido con éxito!",
                              type: "success",
                              showCancelButton: false,
                              confirmButtonColor: "#9bde94",
                              confirmButtonText: "Aceptar",
                              closeOnConfirm: true
                            },function() {
                               location.reload();
                            })                              
                       }                                        
               }
           });
      }else if(consulta == 'U')
          {
           var url = '<?php echo URL?>user/update';
          $.ajax({
             type : 'POST',
             url  : url,
             data : dat,
             dataType : 'json',
             success : function(datos)
               {
                   if(datos.tipo == 'sin_datos')
                       {
                         swal("Error!", "Intente Nuevamente","error");     
                       }                   
               }
           });   
          }
          
   
 });
</script>